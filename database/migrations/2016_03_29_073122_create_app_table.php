<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppTable extends Migration {

	public function up() {

		Schema::create('items', function(Blueprint $table) {

			$table->increments('id');

			$table->string('headline', 150);
			$table->string('description')->nullable();
			$table->string('donator', 30)->nullable();
			$table->string('producer', 30)->nullable();
			$table->string('subject', 30)->nullable();
			$table->integer('zipcode')->unsigned()->nullable();

			$table->integer('dating_from')->unsigned()->nullable();
			$table->integer('dating_to')->unsigned()->nullable();
			$table->integer('received_at')->unsigned();
			$table->integer('created_at')->unsigned();
			$table->integer('updated_at')->unsigned();
			$table->integer('deleted_at')->unsigned()->nullable();

		});

		Schema::create('images', function(Blueprint $table) {

			$table->increments('id');

			$table->integer('item_id')->unsigned();
			$table->foreign('item_id')->references('id')->on('items');
			
			$table->integer('created_at')->unsigned();
			$table->integer('updated_at')->unsigned();
			$table->integer('deleted_at')->unsigned()->nullable();

		});

	}

	public function down() {

		Schema::dropIfExists('images');
		Schema::dropIfExists('items');

	}
}
