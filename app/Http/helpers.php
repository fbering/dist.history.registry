<?php

use Intervention\Image\ImageManagerStatic as Image;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;

if(!function_exists('public_path')) {

	function public_path($p = '') {

		$path = str_replace('app/Http', 'public/', __DIR__);
		return $path.$p;

	}

}

if(!function_exists('save_images')) {

	function save_images($image_url = NULL, $filename = NULL) {
		
		if($image_url === NULL || $filename === NULL)
			return false;

		$image = Image::make($image_url);
		$image->encode('jpg', 100);

		$image->resize(1280, NULL, function($constraint) {
			$constraint->aspectRatio();
			$constraint->upsize();
		});

		// save large version
		$image->save(public_path('img/full/'.$filename.'.jpg'));

		$image->resize(300, NULL, function($constraint) {
			$constraint->aspectRatio();
			$constraint->upsize();
		});

		// save standard version
		$image->save(public_path('img/medium/'.$filename.'.jpg'));

		$image->resize(100, NULL, function($constraint) {
			$constraint->aspectRatio();
			$constraint->upsize();
		});

		// save thumbnail version
		$image->save(public_path('img/thumb/'.$filename.'.jpg'));

		return true;

	}

}

if(!function_exists('config_path')) {

	function config_path($path = '') {

		return app()->basePath() . '/config' . ($path ? '/' . $path : $path);

	}

}




if(!function_exists('create_token')) {

	function create_token($payload = []) {

		$signer = new Sha256();

		$token = (new Builder())->setIssuer(env('BASE_URL')) // iss claim
					->setIssuedAt(time()) // iat claim
					->setExpiration(time() + 3600); // exp claim

		foreach($payload as $k => $pl)
			$token->set($k, $pl);

		$token->sign($signer, env('JWT_SECRET'));
		
		return $token->getToken();

	}

}




if(!function_exists('validate_token')) {

	function validate_token($token) {

		try {

			$signer = new Sha256();
			$token = (new Parser())->parse((string) $token);

			if(!$token->verify($signer, env('JWT_SECRET')))
				return false;

			$data = new ValidationData();
			$data->setIssuer(env('BASE_URL'));

			return $token->validate($data);

		} catch (Exception $e) {

			//echo 'Caught exception: ',  $e->getMessage(), "\n";
			return false;

		}

	}

}
