<?php

$app->group(['prefix' => 'v1/items', 'middleware' => 'auth'], function($app) {

	$app->get('/all', 'App\Http\Controllers\ItemController@all');
	$app->post('/{itemId}', 'App\Http\Controllers\ItemController@update');
	$app->post('/delete/{itemId}', 'App\Http\Controllers\ItemController@destroy');
	$app->delete('/{itemId}', 'App\Http\Controllers\ItemController@destroy');

});

$app->group(['prefix' => 'v1/items'], function($app) {

	$app->get('/', 'App\Http\Controllers\ItemController@index');
	$app->put('/', 'App\Http\Controllers\ItemController@store');
	$app->put('/{itemId}', 'App\Http\Controllers\ItemController@storeImage');
	$app->get('/{itemId}', 'App\Http\Controllers\ItemController@show');

});

$app->get('/v1/login', 'ItemController@login');