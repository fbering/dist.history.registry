<?php 

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Item;
use App\Transformers\ItemTransformer;

use Illuminate\Http\Request;
use Validator;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item as FItem;

class ItemController extends ApiController {

	protected $item;
	protected $request;

	protected $validationRules = [

		'headline' => 'required',

	];

	const SANITY_GOOD = 'GOOD';
	const SANITY_NOTFOUND = 'NOT_FOUND';
	const SANITY_ERROR = 'ERROR';
	const SANITY_VALIDATION_ERROR = 'VALIDATION_ERROR';
	const SANITY_FAILED_AUTH = 'FAILED_AUTH';
	const SANITY_CONTENT_TYPE_ERROR = 'CONTENT_TYPE_ERROR'; 

	function __construct(Item $item) {

		$this->item = $item;

	}

	public function index(Manager $fractal, Request $request, ItemTransformer $itemTransformer) {

		$skip = 0;
		$take = 1000;

		if($request->has('skip'))
			$skip = intval($request->get('skip'));

		if($request->has('take'))
			$take = intval($request->get('take'));

		$items = $this->item->feed($skip, $take);

		if($items->count() <= 0)
			return $this->respondCORS(self::SANITY_NOTFOUND);

		$collection = new Collection($items, $itemTransformer);
		$this->setData($fractal->createData($collection)->toArray());
		return $this->respondCORS();

	}

	public function all(Manager $fractal, Request $request, ItemTransformer $itemTransformer) {

		$items = $this->item->withTrashed()->with('images')->orderBy('created_at', 'DESC')->get();

		//$items = $this->item->all();

		if($items->count() <= 0)
			return $this->respondCORS(self::SANITY_NOTFOUND);

		$collection = new Collection($items, $itemTransformer);
		$this->setData($fractal->createData($collection)->toArray());
		return $this->respondCORS();

	}

	public function show($itemId, Manager $fractal, ItemTransformer $itemTransformer) {

		$item = $this->item->find(intval($itemId));

		if(!$item)
			return $this->respondCORS(self::SANITY_NOTFOUND);

		$fItem = new FItem($item, $itemTransformer);
		$this->setData($fractal->createData($fItem)->toArray());
		return $this->respondCORS();

	}

	public function store(Manager $fractal, Request $request, ItemTransformer $itemTransformer) {

        if(!$this->validateIt($request->all(), $this->validationRules))
			return $this->respondCORS(self::SANITY_VALIDATION_ERROR);

		foreach($request->all() as $k => $v)
			if(in_array($k, $this->item->getFillable()))
				$this->item->{$k} = $v;

		$this->item->save();

		$fItem = new FItem($this->item, $itemTransformer);
		$this->setData($fractal->createData($fItem)->toArray());
		return $this->respondCORS();

	}

	public function storeImage($itemId, Manager $fractal, Request $request, ItemTransformer $itemTransformer) {

		$item = $this->item->find(intval($itemId));

		if(!$item)
			return $this->respondCORS(self::SANITY_NOTFOUND);
		
		$type = $request->header('content-type');

		// check content type is correct
		if($type !== 'image/jpg' && $type !== 'image/jpg' && $type !== 'image/jpeg')
			return $this->respondCORS(self::SANITY_CONTENT_TYPE_ERROR);

		$image = new Image;
		$image->item_id = $itemId;

		if(!$image->save())
			return $this->respondCORS(self::SANITY_ERROR);
		
		save_images(file_get_contents('php://input'), $image->id);

		$fItem = new FItem($item, $itemTransformer);
		$this->setData($fractal->createData($fItem)->toArray());
		return $this->respondCORS();
		
	}

	public function update($itemId, Request $request, Manager $fractal, ItemTransformer $itemTransformer) {

		$item = $this->item->find(intval($itemId));

		if(!$item)
			return $this->respondCORS(self::SANITY_NOTFOUND);

		foreach($request->all() as $k => $v)
			if(in_array($k, $item->getFillable()))
				$item->{$k} = $v;

		$item->save();

		$fItem = new FItem($item, $itemTransformer);
		$this->setData($fractal->createData($fItem)->toArray());
		return $this->respondCORS();

	}

	public function destroy($itemId, Request $request, Manager $fractal, ItemTransformer $itemTransformer) {

		$item = $this->item->withTrashed()->find(intval($itemId));

		if(!$item)
			return $this->respondCORS(self::SANITY_NOTFOUND);

		if($item->trashed())
			$item->restore();
		else
			$item->delete();

		$fItem = new FItem($item, $itemTransformer);
		$this->setData($fractal->createData($fItem)->toArray());
		return $this->respondCORS();

	}

	public function login(Request $request) {

		exec('java -jar ../storage/java/cmdauth.jar '.$request->get('user').' '.$request->get('pass').' 2>&1', $output);

		if($output[0] !== 'SUCCESS: Login var korrekt!')
			return $this->respondCORS(self::SANITY_FAILED_AUTH);

		$token = create_token([

			'name' => $request->get('user'),
			'admin' => true

		]);

		$this->setWebToken((String) $token);
		return $this->respondCORS();

	}

	private function validateIt($r, $vr) {

		$validator = Validator::make($r, $vr);

		if($validator->fails())
			return false;

		return true;

	}

}