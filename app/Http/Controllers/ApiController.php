<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Response as IlluminateResponse;

class ApiController extends Controller {

	protected $statusCode = IlluminateResponse::HTTP_OK;
	protected $data = [];
	protected $sanityCode = 'GOOD';
	protected $webToken = false;

	public function setStatusCode($statusCode) {

		$this->statusCode = $statusCode;
		return $this;

	}

	public function getStatusCode() {

		return $this->statusCode;

	}

	public function getSanityCode() {

		return $this->sanityCode;

	}

	public function setSanityCode($sanityCode) {

		$this->sanityCode = $sanityCode;
		return $this;

	}

	public function getWebToken() {

		return $this->webToken;

	}

	public function setWebToken($webToken) {

		$this->webToken = $webToken;
		return $this;

	}

	public function getAllData() {

		return $this->data;

	}

	public function getData($key = 'default') {

		return $this->data[$key];

	}

	public function setData($data, $key = 'default') {

		if(!isset($data['data']))
			return false;
		
		$this->data[$key] = $data['data'];
		return $this;

	}

	public function respond($sanityCode = false) {

		if($sanityCode)
			$this->setSanityCode($sanityCode);

		$res = [

			'data' => $this->getAllData(), 
			'sanity' => $this->getSanityCode(),
			'token' => $this->getWebToken(),

		];

		return response()->json($res, $this->getStatusCode());
		
	}

	public function respondCORS($sanityCode = false) {

		if($sanityCode)
			$this->setSanityCode($sanityCode);

		$res = [

			'data' => $this->getAllData(), 
			'sanity' => $this->getSanityCode(),
			'token' => $this->getWebToken(),

		];
		
		return response()->json($res, $this->getStatusCode(), $this->setCORSHeaders());
		
	}

	private function setCORSHeaders() {

		$header['Access-Control-Allow-Origin'] = '*';
		$header['Allow'] = 'GET, POST, OPTIONS';
		$header['Access-Control-Allow-Headers'] = 'Origin, Content-Type, Accept, Authorization, X-Request-With';
		$header['Access-Control-Allow-Credentials'] = 'true';
		return $header;

	}

}