<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Http\Request;
use Illuminate\Http\Response as IlluminateResponse;

class AuthMiddleware {
	
	public function handle($request, Closure $next) {

		if(validate_token($request->get('token')))
			return $next($request);
		
		return response()->json([

			'sanity' => 'FAILED_AUTH',
			'token' => $request->get('token'),

		], IlluminateResponse::HTTP_OK, [

			'Access-Control-Allow-Origin' => '*', 
			'Allow' => 'GET, POST, OPTIONS', 
			'Access-Control-Allow-Headers' => 'Origin, Content-Type, Accept, Authorization, X-Request-With', 
			'Access-Control-Allow-Credentials' => 'true'

		]);
		
	}

}
