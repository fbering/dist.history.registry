<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Item extends Model {

	use SoftDeletes;

	protected $dateFormat = 'd-m-Y H:i:s';
	protected $table = 'items';
	protected $fillable = [
	
		'headline',
		'description',
		'donator',
		'producer',
		'subject',
		'zipcode',
		'dating_from',
		'dating_to',
		'received_at',

	];

	protected function getDateFormat() { return 'U'; }

	public function getReleasedAtDateAttribute() {

		return date($this->dateFormat, $this->released_at);

	}

	public function getCreatedAtAttribute($date) {

		return $date;

	}

	public function getUpdatedAtAttribute($date) {

		return $date;

	}

	public function getDeletedAtAttribute($date) {

		return $date;

	}

	public function getDatingToDateAttribute() {

		$unixValue = 0;

		if($this->dating_to !== NULL)
			$unixValue = $this->dating_to;

		return ['pretty' => date($this->dateFormat, $this->dating_to), 'unix' => $unixValue];

	}

	public function getDatingFromDateAttribute() {

		$unixValue = 0;

		if($this->dating_from !== NULL)
			$unixValue = $this->dating_from;

		return ['pretty' => date($this->dateFormat, $this->dating_from), 'unix' => $unixValue];
	}

	public function getReceivedAtDateAttribute() {

		$unixValue = 0;

		if($this->received_at !== NULL)
			$unixValue = $this->received_at;

		return ['pretty' => date($this->dateFormat, $this->received_at), 'unix' => $unixValue];

	}

	public function getCreatedAtDateAttribute() {

		return ['pretty' => date($this->dateFormat, $this->created_at), 'unix' => $this->created_at];

	}

	public function getUpdatedAtDateAttribute() {

		return ['pretty' => date($this->dateFormat, $this->updated_at), 'unix' => $this->updated_at];

	}

	public function scopeFeed($q, $skip = 0, $take = 20) {

		return $q->with('images')->orderBy('created_at', 'DESC')->skip($skip)->take($take)->get();

	}

	public function scopeAll($q) {

		return $q->withTrashed()->with('images')->orderBy('created_at', 'DESC')->get();

	}

	public function images() {

		return $this->hasMany('App\Models\Image', 'item_id', 'id');

	}

}