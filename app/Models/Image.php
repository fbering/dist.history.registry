<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Image extends Model {

	use SoftDeletes;

	protected $dateFormat = 'd-m-y H:i:s';
	protected $table = 'images';
	protected $fillable = [
	
		'id',
		'item_id',

	];

	protected function getDateFormat() { return 'U'; }

	public function getAllSizesAttribute() {

		$d = [];

		$d['thumb'] = env('BASE_URL') . 'img/thumb/' . $this->id . '.jpg';
		$d['medium'] = env('BASE_URL') . 'img/medium/' . $this->id . '.jpg';
		$d['full'] = env('BASE_URL') . 'img/full/' . $this->id . '.jpg';

		return $d;

	}

	public function item() {

		return $this->belongsTo('App\Models\Item', 'id', 'item_id');

	}

}