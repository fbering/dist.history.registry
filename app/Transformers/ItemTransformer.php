<?php 

namespace App\Transformers;

use App\Models\Item;
use League\Fractal\TransformerAbstract;

class ItemTransformer extends TransformerAbstract {

	public function transform(Item $item) {

		$images = [];

		foreach($item->images as $img)
			$images[] = $img->allSizes;

		return [

			'id' => $item->id,
			'headline' => $item->headline,
			'subject' => $item->subject,
			'description' => $item->description,
			'donator' => $item->donator,
			'producer' => $item->producer,
			'zipcode' => $item->zipcode,

			'images' => $images,

			'dating_from' => $item->dating_from_date,
			'dating_to' => $item->dating_to_date,
			'received_at' => $item->received_at_date,
			'created_at' => $item->created_at_date,
			'updated_at' => $item->updated_at_date,
			'deleted_at' => $item->deleted_at,

		];

	}

}